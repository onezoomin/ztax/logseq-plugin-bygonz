# Bygonz Realtime LogSeq Sync 
 - with History Tracking
 - via SADL (Situation Aware Data Log)

**Status**: Hacky PoC

## Features

- slash commands to register blocks for sync publication and subscription
- local-first far-edge data sync via IPFS / IPNS pubsub
- agent aware CRDT design via [bygonz](https://gitlab.com/onezoomin/ztax/ztax/-/tree/trunk/src/bygonz)
- agent auth via [fission-codes webnative SDK](https://github.com/fission-codes/webnative)

## Coming soon
- Query Gates to selectively publish blocks matching specific queries to specific pubsub channels
- Situation Handling for when you and / or your collaborators create situations that might be considered "conflicts"


## Screencast
![Screencast](https://gitlab.com/onezoomin/ztax/logseq-plugin-bygonz/-/wikis/uploads/3f166f9960457e2b934d5e81bf57354d/Screencast_from_2023-02-15_02-35-45__trimmed_.webm)
