import type { DexiePlusParams, Table } from 'bygonz'
import { BygonzDexie, Publication, defaultOptions } from 'bygonz'

import { Logger } from 'logger'
import { isEqual } from 'lodash-es'
import type { AppLogUpdateBroadcastMessage } from 'bygonz/BygonzWebWorker'
import type { BlockParams } from './LogSeqBlock'
import { BlockVM } from './LogSeqBlock'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[Blocks]', performanceEnabled: true })

const stores = {
  Blocks: 'uuid, content, created, modified, owner, modifier',
  // Subscriptions: 'id++, account',
  // Schemes: 'ID, name, created, modified, owner, modifier',
}

const mappings = {
  Blocks: BlockVM,
  // Schemes: SchemeVM,
}
// const subscriptions: Subscription[] = [
//   // new Subscription({
//   //   type: 'ipfs',
//   //   info: {
//   //     // appCreator: appNameObj.creator,
//   //     // appName: appNameObj.name,
//   //     accountName: 'bygonztest',
//   //     // logId: 'testlog-one',
//   //     // ipns: 'bafzaajaiaejcbsu2yiourqrqvwbj6mqix3zn7uiepo2g3dh5ugnan5dmcmsr5exz',
//   //     // ipns: '/record/L2lwbnMvACQIARIgidd7fVrKmjvZnBDq1b7w_c9miOEzfeV8r-eMUZGc2nU',
//   //     ipns: '/record/L2lwbnMvACQIARIgyprCHUjCMK2CnzIIvvLf0QR7tG2M_aGaBvRsEyUekvk',
//   //   },
//   // }),
//   // new Subscription({
//   //   type: 'ipfs',
//   //   info: {
//   //     // appCreator: appNameObj.creator,
//   //     // appName: appNameObj.name,
//   //     accountName: 'work-profile',
//   //   },
//   // }),
// ]
const publications: Publication[] = [
  new Publication({
    type: 'ipfs',
    info: {
      // appCreator: appNameObj.creator,
      // appName: appNameObj.name,
      accountName: 'bygonztest',
      // logId: 'testlog-one',
      query: '*',
    },
  }),
]
const options = {
  ...defaultOptions,
  // prePopulateTables: {
  //   Blocks: initialBlocks,
  // },
  // subscriptions,
  publications,
}

export class LogseqDB extends BygonzDexie {
  // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
  // {entity}Params | {entity}VM allows for partial objects to be used in add and put and for the class of retrieved entities to include getters
  public Blocks: Table<BlockParams | BlockVM, string> | undefined
  // public Subscriptions: Table<{id?: number, info: {accountName: string}, fileCount?: number}, number>
  // public Schemes: Table<TaskParams | TaskVM, CompoundKeyNumStr>

  // public Subscriptions: Table<{id?: number, info: {accountName: string}, fileCount?: number}, number>
  // public Schemes: Table<TaskParams | TaskVM, CompoundKeyNumStr>

  async init(userAddress) {
    this.options.activeAgent = userAddress
    if (self.document !== undefined)
      await this.setup(userAddress) // in super
  }

  constructor(...params: DexiePlusParams) {
    super(...params)
    this.doMappings()
    // super(params[0]) // reactivity works if extending Dexie (not loaded from CDN) and using these normal instantiations
    // this.version(1).stores(stores)
  }

  async updateDBOnNewAppLogs(data: AppLogUpdateBroadcastMessage) {
    const { entityArray, entitiesGroupedByTablename } = await this.getEntitiesAsOf()
    DEBUG('[updateDBOnNewAppLogs]:', { entityArray, entitiesGroupedByTablename })
    if (Object.keys(entitiesGroupedByTablename).length === 0) {
      if (entityArray.length)
        WARN('No tablename grouping but entities:', { entityArray, entitiesGroupedByTablename })
      return
    }
    if (!isEqual(Object.keys(entitiesGroupedByTablename), ['Blocks']))
      throw new Error(`Unexpected table names: ${Object.keys(entitiesGroupedByTablename).join(',')}`)

    await DEBUG.group('Updating LogseqDB', { data, entitiesGroupedByTablename, entityArray }, async () => {
      await this.nonBygonzDB.transaction('rw', this.nonBygonzDB._allTables.Blocks, async () => {
        await this.nonBygonzDB._allTables.Blocks.bulkPut(entitiesGroupedByTablename.Blocks)
      })
      // this.sendAtomsToWasm()
    })
  }

  // eslint-disable-next-line @typescript-eslint/require-await
  async onError(error) {
    ERROR('Bygonz error :', error)
    void logseq.UI.showMsg(`Bygonz error: ${error?.message ?? JSON.stringify(error)}`, 'error')
  }
}

let logseqDB: LogseqDB
export const getInitializedLogseqDB = async () => {
  if (logseqDB)
    return logseqDB
  else throw new Error('Uninitialized')
}
export const initializeLogseqDB = async () => {
  if (logseqDB)
    throw new Error('Already initialized')
  const userAddress = logseq.settings?.fission_username ?? ''
  const bygonzOptions = {
    ...options,
    activeAgent: userAddress,
    ipfs: { rpcUrl: logseq.settings?.ipfs_rpc_url },
  }
  DEBUG('Creating Bygonz LogseqDB', { bygonzOptions, stores, mappings })
  logseqDB = new LogseqDB('BygonzLogseq', stores, mappings, bygonzOptions)
  VERBOSE('blocks db coming', logseqDB)

  DEBUG('Initializing Bygonz LogseqDB', logseqDB)
  await logseqDB.init(userAddress)
  return logseqDB
}

export async function getAllBlockVMs() {
  DEBUG('DB:', logseqDB)
  const entitiesResult = await logseqDB.getEntitiesAsOf()
  DEBUG('LogseqDB entities:', { entitiesResult })
  const blockVMs: BlockVM[] = entitiesResult.entityArray
  // bygonz should take care of this mapping actually .map(eachUncastBlockObj => new BlockVM(eachUncastBlockObj))
  return blockVMs
}

if (import.meta.hot) {
  import.meta.hot.accept('bygonz', (newFoo) => {
    LOG('BYGONZ MODULE RELOAD', newFoo)
    import.meta.hot?.invalidate()
  })
}
