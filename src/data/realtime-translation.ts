import { BlockEntity, IDatom } from '@logseq/libs/dist/LSPlugin'
import { allPromises } from 'bygonz'
import { groupBy, mapValues, maxBy, partition } from 'lodash-es'
import { Logger } from 'logger'
import { mapBlockToBlockVM, mapBlockValueToBygonzValue } from './blocks-to-bygonz'
import { LogseqDB } from './bygonz'
import { bygonzMutex } from './data-utils'
import { BlockParams, BlockVM } from './LogSeqBlock'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[DB]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars
type ChangeEvent = Parameters<Parameters<typeof logseq.DB.onChanged>[0]>[0]
interface AddOp {
  op: 'add'
  data: BlockParams
}
interface UpdateOp {
  op: 'update'
  bygonzID: string
  data: Partial<BlockParams>
}
type ChangeOp = AddOp | UpdateOp

export async function handleDBChangeEvent (event: ChangeEvent, logseqDB: LogseqDB) {
  const {
    blocks,
    txData, // entityID, attribute, value, transaction, op(false=before,true=after)
    txMeta,
  } = event
  if (!blocks) return DEBUG('Dropping changeEvent without blocks:', event)
  if (!logseqDB) return WARN('logseqDB is missing - Dropping changeEvent:', event)

  VERBOSE(`handleChange triggered${txMeta?.outlinerOp ? ` (op=${txMeta?.outlinerOp})` : ''}`, event)
  const unlockMutex = await bygonzMutex.lock()
  // DEBUG(`handleChange${txMeta?.outlinerOp ? ` (op=${txMeta?.outlinerOp})` : ''}`, event)
  
  try {
    // Find all relevant IDs to check if the change is related to our known blocks
    const ids = (await Promise.all(blocks.map(async b => {
      if (!b.uuid) return []
      if (!b.parent) return [b.uuid]
      const block = (await logseq.Editor.getBlock((b.parent).id))
      if (!block) return [b.uuid]
      const parentUuid = block.properties?.bygonz ?? block.uuid
      return [b.uuid, parentUuid]
    }))).flat()
    VERBOSE('Matches any known block?', { ids, blocks })
    const matches = await logseqDB.Blocks?.where('uuid').anyOf(ids).count() ?? 0
    VERBOSE('Known block matches:', matches, { blocks })
    if (!matches) return

    return await DEBUG.group(`handleChange details${txMeta?.outlinerOp ? ` (op=${txMeta?.outlinerOp})` : ''}`, event, async () => {
      if (!txData) return

      const changeSets: ChangeOp[] = []

      if (txMeta?.outlinerOp === 'deleteBlocks') {
        for (const block of blocks) {
          const bygonzID = block.properties?.bygonz ?? block.uuid // TODO: isn't this outdated?
          if (!bygonzID) { ERROR('deleted', block); throw new Error('Failed to get bygonzID from deleted block') }
          changeSets.push({ op: 'update', bygonzID, data: { isDeleted: true } })
          // TODO: should this also happen when the root is deleted? (or more specifically when a whole bunch of blocks including a bygonz root is deleted)
        }
      } else {
        // Index data
        const atomsByEntityID = groupBy(txData, '[0]')
        const entitiesByID = mapValues(groupBy(blocks, 'id'), matches => {
          if (matches.length > 1) throw new Error('Multiple blocks with same ID')
          return matches[0]
        })
        DEBUG({ atomsByEntityID, entitiesByID })
        // Iterate over individual entities
        for (const [entityID, atoms] of Object.entries(atomsByEntityID)) {
          const entity = entitiesByID[entityID]
          const changeSet = await mapAtomsToDBChangeSet(entity, atoms, entityID, txMeta, logseqDB)

          if (changeSet) changeSets.push(changeSet)

          // if (txMeta?.outlinerOp === 'saveBlock') {
          //   const contentChanges = atomsForAttribute('content')
          // } else if (txMeta?.outlinerOp === 'moveBlocks') {
        }
      }

      const { Blocks: BlocksTable } = logseqDB
      if (changeSets.length && BlocksTable) {
        console.groupCollapsed('[DB transaction]', { changeSets })
        try {
          await logseqDB.transaction('rw', BlocksTable, async () => {
            for (const dbOp of changeSets) {
              DEBUG('saving changeSet', dbOp)
              if (dbOp.op === 'add') {
                await BlocksTable.add(dbOp.data)
              } else {
                await BlocksTable.update(dbOp.bygonzID, dbOp.data)
              }
            }
          })
        } catch (err) {
          ERROR(err)
        } finally {
          console.groupEnd()
        }
      }
    })
  } finally {
    unlockMutex()
  }
}
async function mapAtomsToDBChangeSet (
  block: BlockEntity,
  atoms: IDatom[],
  entityID: string,
  txMeta: { [key: string]: unknown, outlinerOp: string } | undefined,
  logseqDB: LogseqDB,
): Promise<ChangeOp | null> {
  if (!entityID) {
    WARN('Ignoring change op without bygonzID', { block, atoms })
    return null
  }
  if (!block.parent || !block.page) {
    WARN('Ignoring block without page or parent:', { block })
    return null
  }
  const { Blocks: BlocksTable } = logseqDB
  if (!BlocksTable) {
    throw new Error('missing BlocksTable bailing')
  }
  const bygonzID = block.properties?.bygonz ?? block.uuid

  const existingVM = await BlocksTable.get(bygonzID)

  if (!existingVM) {
    const blockVM = await mapBlockToBlockVM(bygonzID, block)
    DEBUG('Change event for non-existent VM... creating', { bygonzID, block, blockVM })
    return { op: 'add', data: blockVM }
  } else {
    const atomsByAttribute = groupBy(atoms, '[1]')
    const atomsForAttribute = (attr: string) => {
      const atomsForAttr = atomsByAttribute[attr] // TODO consider .filter(eachAtom=>eachAtom[4])
      if (atomsForAttr.length === 0) { return [[], []] }

      const [after, before] = partition(atomsForAttr, '[4]') // partition by op (if false, it's the retraction)
      // .map(list => list.map(atom => atom[2])) // for each atoms list get just the value of the atom
      DEBUG('[DB.saveBlock] content:', [before, after])
      // if (before.length > 1) { WARN('Hm... retraction count > 1:', attr, before, { entityID, atoms }) }
      // if (after.length !== 1) { ERROR('WTF?  new value count != 1:', attr, after, { entityID, atoms }) }
      if (after.length !== before.length) { ERROR('WTF?  after.length !== before.length:', attr, { before, after, entityID, atoms }) }
      return [before, after]
    }
    DEBUG('atomsByAttribute', atomsByAttribute)

    const attrWhitelist = ['content', 'parent', 'left']
    const attrsToCheck = Object.keys(atomsByAttribute).filter(a => attrWhitelist.includes(a))
    const changeSet: Partial<BlockVM> = {}
    for (const attr of attrsToCheck) {
      const [before, after] = atomsForAttribute(attr)
      // DEBUG(`Changed? '${attr}' `, after, { before, entity, bygonzID })
      // if (after.length) {
      const dbBefore = (await BlocksTable?.get(bygonzID))?.[attr]
      DEBUG(`Saving new '${attr}' value:`, after, { entity: block, bygonzID })
      if (!((await allPromises(before.map(async atom => await mapBlockValueToBygonzValue(attr, atom[2])))).includes(dbBefore))) {
        WARN('vm state was different than \'before\' of DB change:', { before, dbBefore, bygonzID, entity: block })
      }
      const newestAtom = maxBy(after, '[3]')
      if (!newestAtom) {
        WARN(`no new value for '${attr}':`, { before, after })
      } else {
        const newVal = newestAtom[2]
        DEBUG(`Mapping value for '${attr}':`, { newestAtom, newVal })
        const mappedNewVal = await mapBlockValueToBygonzValue(attr, newVal)
        DEBUG(`Mapped value for '${attr}':`, { newestAtom, newVal, mappedNewVal })
        if (mappedNewVal) {
          changeSet[attr] = mappedNewVal
        }
      }
      // }
    }

    return { op: 'update', bygonzID, data: changeSet }
  }
}

// ------------- //
// Discarded PoC //
// ------------- //
// export async function applyAppLogs (appLogs: AppLogObj[]) {
//   DEBUG('Applying new applogs:', appLogs)
//   const atomsByEntityID = groupBy(appLogs, 'en')
//   for (const [entityID, atoms] of Object.entries(atomsByEntityID)) {
//     const atomsByAttribute = groupBy(atoms, 'at')
//     DEBUG('atomsByAttribute', atomsByAttribute)
//     //   const attrWhitelist = ['content', 'parent', 'left'] - can also just save all for now
//     const attrsToCheck = Object.keys(atomsByAttribute)//.filter(a => attrWhitelist.includes(a))
//     for (const attr of attrsToCheck) {
//       const newValues = atomsByAttribute[attr]
//       if (newValues.length > 1) WARN(`More than one change of ${attr}, choosing last`, { newValues, atoms }) // HACK: WANTED: situation handling?
//       const newValue = newValues[newValues.length - 1]
//       DEBUG(`Saving new '${attr}' value:`, newValue, { entity, bygonzID })
//       if (attr === 'content') {
//         await logseq.Editor.updateBlock(...)
//       }
//     }
//   }
// }

if (import.meta.hot) {
  import.meta.hot.accept('bygonz', (newFoo) => {
    console.log('BYGONZ MODULE RELOAD', newFoo)
    import.meta.hot?.invalidate()
  })
}
