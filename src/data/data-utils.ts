import Mutex from 'await-mutex'
import { Logger } from 'logger'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const bygonzMutex = new Mutex()
let _blockTreeModificationSemaphore = 0
export const isModifyingBlocktree = () => _blockTreeModificationSemaphore

export async function modifyBlocksWithoutChangeHandler<T>(func: () => Promise<T>): Promise<T> {
  DEBUG('[modifyBlocksWithoutChangeHandler] awaiting mutex')
  const unlockMutex = await bygonzMutex.lock()
  try {
    _blockTreeModificationSemaphore++
    return await func()
  }
  finally {
    DEBUG('[modifyBlocksWithoutChangeHandler] releasing mutex')
    unlockMutex()
    setTimeout(() => { // HACK deferred to prevent race conditions because the update from Logseq is not bound to this single-thread situation we call JavaScript
      _blockTreeModificationSemaphore--
    }, 1000)
  }
}
