// import keystore from 'keystore-idb'
import { Logger } from 'logger'
import * as wn from 'webnative'
import { Maybe } from 'webnative'
import type { Ucan } from 'webnative/ucan/types'

const { Branch } = wn.path

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[WN]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

let session: Maybe<wn.Session> = null
let program: wn.Program
const ucanDecoder = wn.ucan.decode
const ucanSigner = wn.ucan.sign
const userChoseToRegister = false
export async function initWebnative (username) {
  if (!username) throw new Error(`bogus username: ${JSON.stringify(username)}`)
  try {
    LOG('initializing webnative')
    program = await wn.program({
      // Can also be a string, used as an identifier for caches.
      // If you're developing multiple apps on the same localhost port,
      // make sure these differ.
      namespace: { creator: 'ztax', name: 'bygonz_logseq' },
      debug: true,
    })
  } catch (error) {
    ERROR('program init error', error)
    switch (error) {
      case wn.ProgramError.InsecureContext:
        // wn requires HTTPS
        break
      case wn.ProgramError.UnsupportedBrowser:
        break
    }
  }
  LOG('We have a program 😎', program)
  let fs, keystore
  // Do we have an existing session?
  if (program.session) {
    session = program.session
    LOG('We have a session 🎉', session, { Branch })
    fs = session.fs
    keystore = session.fs?.dependencies.crypto.keystore
    // If not, let's authenticate.
    // (a) new user, register a new Fission account
  } else if (userChoseToRegister) {
    const { success } = await program.auth.register({ username })
    session = success ? await program.auth.session() : null
    LOG('We registered and now have a session 🎉', session)

    // (b) existing user, link a new device
  } else {
    // On device with existing session:
    // const producer = await program.auth.accountProducer(program.session.username)

    // producer.on('challenge', challenge => {
    // // Either show `challenge.pin` or have the user input a PIN and see if they're equal.
    //   if (userInput === challenge.pin) challenge.confirmPin()
    //   else challenge.rejectPin()
    // })

    // producer.on('link', ({ approved }) => {
    //   if (approved) console.log('Link device successfully')
    // })

    // On device without session:
    //     Somehow you'll need to get ahold of the username.
    //     Few ideas: URL query param, QR code, manual input.
    const consumer = await program.auth.accountConsumer(username)

    consumer.on('challenge', ({ pin }) => {
      LOG('Connection PIN:', pin)
    })

    consumer.on('link', ({ approved, username }) => {
      void (async () => {
        if (approved) {
          DEBUG(`Successfully authenticated as ${username} 🥳`)
          session = await program.auth.session()
          DEBUG(`Got session for ${username}:`, session)
        }
      })().catch(ERROR)
    })
  }

  setTimeout(() => {
    (async () => {
      WARN('STARTING SHENANIGANS!')
      if (!session) throw new Error('No sessioin')
      const fs = session.fs
      if (!fs) throw new Error('No sessioin.fs')

      // LOG('Ls public:', await fs.ls(
      //   wn.path.directory(Branch.Public),
      // ))
      const contentPath = wn.path.file(
        Branch.Public, 'hello.txt',
      )
      LOG('Writing', contentPath)
      try {
        await fs.write(
          contentPath,
          new TextEncoder().encode(`👋 ${new Date().toISOString()}`), // Uint8Array
        )
      } finally {
        await proofCheck(fs)
      }
      // Persist changes and announce them to your other devices
      LOG('Publishing...')
      await fs.publish()

      // Read the file
      LOG('Reading again...')
      const encodedContent = await fs.read(contentPath)
      const content = encodedContent && new TextDecoder().decode(
        encodedContent,
      )
      LOG('Read again:', { content, encodedContent })
    })().catch(console.error)
  }, 100)
  DEBUG('returning', { fs, keystore })
  return { fs, keystore }
}

async function proofCheck (fs) {
  const proofs = { ...fs?.proofs }
  const proofValues = Object.values(proofs)
  const proofZero: Ucan = proofValues[0]

  const payload = proofZero?.payload
  const fscrypto = fs.dependencies.crypto
  Object.assign(window, { ucanDecoder, ucanSigner, wnfs: fs, fscrypto }) // HACK
  DEBUG({ fs, proofs, proofValues, proofZero, payload, ucanDecoder, ucanSigner })
  if (payload && proofZero) {
    const {
      header: decodedHeader,
      payload: decodedPayload,
      signature: decodedSig,
    } = ucanDecoder(payload.prf)
    const sigCheck = await ucanSigner(fscrypto, proofZero.header, proofZero.payload)
    const isProofZeroSigValid = sigCheck === proofZero.signature
    let sigCheckInner, sigCheckDeepInner, decodedInnerPayload, decodedInnerSig
    if (typeof decodedPayload?.prf === 'string') {
      const {
        header: innerHeader,
        payload: innerPayload,
        signature: innerSig,
      } = ucanDecoder(decodedPayload?.prf)
      decodedInnerPayload = innerPayload
      decodedInnerSig = innerSig
      sigCheckInner = await ucanSigner(fscrypto, decodedHeader, decodedPayload)
      sigCheckDeepInner = await ucanSigner(fscrypto, innerHeader, innerPayload)
      // TODO find a way to reproduce the inner sig... so far no dice 🎲
    }
    DEBUG({ isProofZeroSigValid, sigCheck, decodedHeader, decodedPayload, decodedSig, sigCheckInner, decodedInnerPayload, decodedInnerSig, sigCheckDeepInner })
  }
}
