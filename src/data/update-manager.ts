import type { BlockEntity, IDatom, IHookEvent } from '@logseq/libs/dist/LSPlugin'

import '@logseq/libs'

// import './index.css'

import type { AppLogVM } from 'bygonz'
import { debounce, groupBy } from 'lodash-es'
import { Logger } from 'logger'
import type { AppLogUpdateBroadcastMessage } from 'bygonz/BygonzWebWorker'
import { INLINE_BLOCK_MACRO, emitter, getBlockIfUUID } from '../ui/ui-utils'
import { checkDBChangeForBygonzUrl } from '../ui/slash-commands'
import type { LogseqDB } from './../data/bygonz'
import { getAllBlockVMs, getInitializedLogseqDB, initializeLogseqDB } from './../data/bygonz'
import type { BlockVM } from './../data/LogSeqBlock'

// import { initIPFS, loadBlockFromIPFS } from './data/ipfs'
import { initiateLoadFromBlock, saveBlockRecursively } from './../data/blocks-to-bygonz'
import { handleDBChangeEvent } from './../data/realtime-translation'
import { initWebnative } from './webnative'
import { isModifyingBlocktree, modifyBlocksWithoutChangeHandler } from './data-utils'
import { wrapWithCatchAndDisplayError } from '@/utils'

type DBchangeEvent = IHookEvent & { blocks: BlockEntity[]; txData: IDatom[]; txMeta?: { [key: string]: any; outlinerOp: string } | undefined }

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true })

export let state: 'uninitialized' | 'loading' | 'pulling' | 'ready' | 'disabled' | 'warning' = 'uninitialized'
const stateListeners: Array< (state: string) => void | Promise<void>> = []
export function registerStateListener(func: (state: string) => void) {
  stateListeners.push(func)
}
export function setState(s: typeof state) {
  state = s
  void (async () => {
    for (const listener of stateListeners)
      await listener(state)
  })
}

// eslint-disable-next-line import/no-mutable-exports
export let logseqDB: LogseqDB | null = null
// eslint-disable-next-line import/no-mutable-exports
export let wnfs: FileSystem | null = null

let isDBchangeListenerActive = false
// eslint-disable-next-line import/no-mutable-exports
export let acknowlegedRoots: Set<string> = new Set()

export const toggles = {
  realtime: true,
  changeListener: false,
}

let debounceCallsBeforeFlush = 0
const mergedOnChangeEventQue: Record<string, DBchangeEvent> = {}
const debouncedDBchangeQueFlush = debounce(() => {
  if (!logseqDB)
    throw new Error('logseqDB not initialized')
  for (const [eachOutlinerOp, eachMergedEvent] of Object.entries(mergedOnChangeEventQue)) {
    DEBUG('debounced DBchange', { debounceCallsBeforeFlush, eachMergedEvent })
    void handleDBChangeEvent(eachMergedEvent, logseqDB).catch(err => ERROR('onDBChange error', { err, eachOutlinerOp }))
    delete mergedOnChangeEventQue[eachOutlinerOp] // eslint-disable-line @typescript-eslint/no-dynamic-delete
    debounceCallsBeforeFlush = 0
  }
}, 1234)

export async function setupBygonzLogseqDB() {
  DEBUG('Init LogseqDB')
  const loadedLogseqDB = logseqDB = await initializeLogseqDB()
  loadedLogseqDB.addUpdateListener(async ({ added }) => {
    await bygonzLoad({ fromBackground: true, newAppLogs: added })
  })

  const userHandle = logseq.settings?.fission_username
  if (userHandle) {
    const { fs, keystore } = (await initWebnative(userHandle)) ?? {}
    wnfs = fs ?? WARN('bogus fs', fs)
    keystore && loadedLogseqDB.setActiveAgentKeystore(keystore)
  }

  emitter.on('bygonzLoad', () => {
    // void logseq.UI.showMsg('running bygonzLoad() ...', 'info', {
    //   timeout: 1500,
    // })
    bygonzLoad().catch(ERROR)
  })
  emitter.on('bygonzReset', wrapWithCatchAndDisplayError(async () => {
    await bygonzReset()
    void logseq.UI.showMsg('Bygonz reset - RELOAD PLEASE 💁', 'info', {
      // timeout: 1500,
    })
  }, 'Bygonz reset'))
}

export async function bygonzReset() {
  await indexedDB.deleteDatabase('BygonzLogseq')
  await indexedDB.deleteDatabase('_BygonzLogseq')
  // window.parent.location.reload()
  // await logseq.App.relaunch()
  WARN('DBs deleted - RELOAD PLEASE 💁')
}

export async function bygonzSaveBlock(blockOrUUID: string | BlockEntity) {
  const block = await getBlockIfUUID(blockOrUUID)
  await logseq.Editor.exitEditingMode(true)
  // await sleep(500) // HACK otherwise root node content might not update

  const loadedLogseqDB = logseqDB = logseqDB ?? await getInitializedLogseqDB()
  DEBUG('Pinning root UUID & bygonz tag:', block)
  await logseq.Editor.upsertBlockProperty(block.uuid, 'id', block.uuid)
  // await logseq.Editor.upsertBlockProperty(block.uuid, 'bygonz', block.uuid)
  await saveBlockRecursively(block, loadedLogseqDB)

  acknowlegedRoots.add(block.uuid)

  // logseq.DB.onBlockChanged(currentBlock.uuid,
  //   (block: BlockEntity, txData: IDatom[], txMeta?: { [key: string]: any, outlinerOp: string } | undefined) => {
  //     DEBUG('onBlockChanged:', { block, txData, txMeta })
  //     saveBlockRecursively(block, loadedLogseqDB)
  //   },
  // )
}

export async function bygonzLoad({ currentBlock = null, fromBackground = false, newAppLogs = undefined }: { currentBlock?: BlockEntity | null; fromBackground?: boolean; newAppLogs?: AppLogVM[] } = {}) {
  LOG('SYNC init 🎉', { currentBlock, fromBackground, newAppLogs })
  await logseqDB!.updateDBOnNewAppLogs({} as AppLogUpdateBroadcastMessage)
  const blockVMs: BlockVM[] = await getAllBlockVMs() // these should be mapped to VMs from the bygonz fx
  DEBUG('[bygonzLoad]', { blockVMs, singleBlockHistory: await blockVMs[0]?.getEntityHistory?.() })
  const stateBefore = state
  setState('pulling')
  try {
    await modifyBlocksWithoutChangeHandler(async () => {
      let rootBlocks: Array<Array<{ uuid: string/* , properties: Record<string, any> */ }>>
      try {
        rootBlocks = await logseq.DB.datascriptQuery(`
        [:find (pull ?b [:block/uuid])
          :where
            [?b :block/content ?content]
            [(clojure.string/includes? ?content "${INLINE_BLOCK_MACRO}")]]
        ]`)/*
          [?b :block/properties ?prop]
          [(get ?prop :bygonz) ?v] */
      }
      catch (err) { // logseq error messages are missing a stacktrace, so we have to catch it as early as possible
        ERROR('Failed to find rootBlocks', err)
        throw new Error('Failed to find bygonz root blocks')
      }
      DEBUG('All bygonz roots:', rootBlocks)
      acknowlegedRoots = new Set(rootBlocks.map(([b]) => b.uuid))
      for (const [root] of rootBlocks) {
        const blockVM = blockVMs.find(b => b.uuid === root.uuid /* .properties.bygonz */)
        if (!blockVM) {
          WARN('Did not find Bygonz root in DB:', root)
          continue
        }
        DEBUG('Updating root:', { root, blockVM })
        await initiateLoadFromBlock(root, blockVMs)
      }

      LOG('SYNC done 🎉')
    })
  }
  finally {
    setState(stateBefore)
  } // HACK: workaround or change events sent off by our load
}

export async function setupDBchangeListener() {
  if (isDBchangeListenerActive) {
    DEBUG('setupDBchangeListener called - avoiding adding listener again')
  }
  else {
    const loadedLogseqDB = logseqDB = logseqDB ?? await getInitializedLogseqDB()

    logseq.DB.onChanged((event) => {
      const {
        blocks,
        txData, // entityID, attribute, value, transaction, op(false=before,true=after)
        txMeta,
      } = event
      VERBOSE('onChange listener', event, toggles)
      if (!toggles.realtime)
        return VERBOSE('Skipping ChangeEvent as toggles.realtime=false')
      if (isModifyingBlocktree())
        return WARN('Skipping ChangeEvent as isModifyingBlocktree=true', event) // HACK: find better way to check if we did this

      if (txMeta?.outlinerOp) {
        if (mergedOnChangeEventQue[txMeta?.outlinerOp]) {
          const blocksByID = groupBy(mergedOnChangeEventQue[txMeta?.outlinerOp].blocks, 'id')

          for (const eachBlock of blocks) {
            if (Object.keys(blocksByID).includes(eachBlock.id.toString())) {
              // TODO replace array entry with more recent version (updated time will be different)
            }
            else {
              mergedOnChangeEventQue[txMeta?.outlinerOp].blocks.push(eachBlock)
            }
          }
          mergedOnChangeEventQue[txMeta?.outlinerOp].txData.push(...txData)
        }
        else {
          mergedOnChangeEventQue[txMeta?.outlinerOp] = { blocks, txData, txMeta }
        }
        // mergedOnChangeEventQue[txMeta?.outlinerOp].txMeta = txMeta // TODO check if txMeta.transact? is ever false
      }

      DEBUG('onChange listener', event, toggles, mergedOnChangeEventQue)
      // void handleDBChangeEvent(event, logseqDB).catch(err => ERROR('onDBChange error', err))
      debouncedDBchangeQueFlush()
      debounceCallsBeforeFlush++

      void checkDBChangeForBygonzUrl(txMeta?.outlinerOp, blocks, loadedLogseqDB)
    })
    isDBchangeListenerActive = true
  }
}

if (import.meta.hot) {
  import.meta.hot.accept('bygonz', (newFoo) => {
    DEBUG('BYGONZ MODULE RELOAD', newFoo)
    import.meta.hot?.invalidate()
  })
}
