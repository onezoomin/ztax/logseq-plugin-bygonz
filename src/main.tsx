import { Buffer } from 'buffer'
import type { SettingSchemaDesc } from '@logseq/libs/dist/LSPlugin'

import '@logseq/libs'
import { Logger } from 'logger'
import { logseq as PL } from '../package.json'

import { setState, setupBygonzLogseqDB, setupDBchangeListener } from './data/update-manager'
import { registerInlineUI } from './ui/inline-ui'
import { registerMainModal } from './ui/main-modal'
import { registerToolbarItems } from './ui/toolbar'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

globalThis.Buffer = Buffer

const settings: SettingSchemaDesc[] = [
  {
    title: 'Bygonz Sync Interval',
    key: 'Bygonz Sync Interval',
    type: 'number',
    default: 60,
    description: 'Set the length of time between sync polling in seconds',
  },
  {
    title: 'Fission Username',
    key: 'fission_username',
    type: 'string',
    default: '',
    description: 'Set Fission User Name',
  },
  {
    title: 'IPFS RPC url',
    key: 'ipfs_rpc_url',
    type: 'string',
    default: 'http://127.0.0.1:5001/',
    description: 'IPFS Daemon RPC API url',
  },
]

const pluginId = PL.id

export async function main() {
  await LOG.group(`#${pluginId}: MAIN`, async () => {
    logseq.useSettingsSchema(settings)

    // logseq.provideModel({
    //   bygonzReset,
    //   bygonzLoad,
    //   bygonzSave,
    // })

    setState('loading')

    registerToolbarItems()
    registerInlineUI()
    registerMainModal()
    await setupBygonzLogseqDB()
    await setupDBchangeListener()
    // logseq.App.onBlockRendererSlotted((...args) => {
    //   ERROR('RESURRECTED onBlockRendererSlotted?!', args)
    // })

    setState('ready')
  })
}

logseq.ready(main).catch(ERROR)

// const interval = setInterval(() => DEBUG('alive'), 1000)
// const top_ref = top
// if (import.meta.hot) { // https://vitejs.dev/guide/api-hmr.html
//   // DEBUG('[logseq-bygonz] 🤖 HMR support detected', import.meta.hot)
//   import.meta.hot.accept((_newModule) => {
//     // DEBUG('[logseq-bygonz] 🤖 Hot reload detected - Triggering reload', newModule)
//     void (async () => {
//       console.log('%c✨ HMR update ready - reloading ✨', 'font-weight: bold; font-size: 15px; color: purple;')
//       if (!top_ref) return WARN('HMR fail - no top')
//       try {
//         await top_ref.LSPluginCore.reload('_bygonz')
//       } catch (err) { DEBUG('HMR error (probably double HMR race):', err); return }

//       // DEBUG('[logseq-bygonz] 🤖 TRIGGER PAGE RELOAD x1', top_ref)
//       top_ref.eval(`(() => {
//         console.log("✨ Post-HMR -> RELOADING PAGE ✨");
//         let name = logseq.api.get_current_page().originalName;
//         logseq.api.replace_state("home");
//         setTimeout(() => logseq.api.replace_state("page", { name })); // sometimes it works without defer, but sometimes it doesn't
//       })();`)
//     })()

//     // don't actually hot reload, do a cold reload - we just wanted to trigger a plugin reload when the new module is ready
//     import.meta.hot!.invalidate()
//   })

//   import.meta.hot.dispose((_newModule) => {
//     // DEBUG('[logseq-bygonz] 🤖 Hot reload Disposing old', newModule)
//     // clearInterval(interval)
//   })
// }

if (!navigator.userAgent.includes('Electron')) {
  LOG('NOT electron - launching main...')
  main().catch(ERROR)
}

if (import.meta.hot) {
  import.meta.hot.accept('bygonz', (newFoo) => {
    LOG('BYGONZ MODULE RELOAD', newFoo)
    import.meta.hot?.invalidate()
  })
}
