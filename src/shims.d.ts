import type { AttributifyAttributes } from '@unocss/preset-attributify'


// Unocss attributify - https://github.com/unocss/unocss/tree/main/packages/preset-attributify#typescript-support-jsxtsx
declare module '@vue/runtime-dom' {
  interface HTMLAttributes {
    [key: string]: any
  }
}
declare module '@vue/runtime-core' {
  interface AllowedComponentProps {
    [key: string]: any
  }
}
declare module '@vue/runtime-dom' {
  interface HTMLAttributes extends AttributifyAttributes {}
}

export {}
