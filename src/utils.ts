import type { BlockEntity, LSPluginUserEvents } from '@logseq/libs/dist/LSPlugin.user'
import { get } from 'lodash-es'
import React from 'react'
import { Logger } from 'logger'
import type { BlockWithChildren } from './data/blocks-to-bygonz'

export type Overwrite<T, O> = Omit<T, keyof O> & O

let _visible = logseq.isMainUIVisible
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const css = (t, ...args) => String.raw(t, ...args)

function subscribeLogseqEvent<T extends LSPluginUserEvents>(
  eventName: T,
  handler: (...args: any) => void,
) {
  logseq.on(eventName, handler)
  return () => {
    logseq.off(eventName, handler)
  }
}

const subscribeToUIVisible = (onChange: () => void) =>
  subscribeLogseqEvent('ui:visible:changed', ({ visible }) => {
    _visible = visible
    onChange()
  })

export const useAppVisible = () => {
  return React.useSyncExternalStore(subscribeToUIVisible, () => _visible)
}

export function flatMapRecursiveChildren(block: BlockWithChildren): BlockEntity[] {
  return [block, ...block.children.flatMap(flatMapRecursiveChildren)]
}

export async function catchAndDisplayError<R>(func: () => Promise<R>, context: string): Promise<R | Error> {
  try {
    return await func()
  }
  catch (err) {
    ERROR(err)
    await logseq.UI.showMsg(`${context} - ${get(err, 'message') ?? JSON.stringify(err)}`, 'error')
    return err instanceof Error ? err : new Error(`${err}`)
  }
}

export function wrapWithCatchAndDisplayError<ARGS extends any[]>(func: (...args: ARGS) => Promise<void>, context: string): (...args: ARGS) => void {
  return (...args) => {
    void catchAndDisplayError(async () => {
      return await func(...args)
    }, context)
  }
}
