import '@logseq/libs'

import { validate as isValidUUID } from 'uuid'
// import './index.css'

// import { initIPFS, loadBlockFromIPFS } from './data/ipfs'
import { Subscription } from 'bygonz'
import { Logger } from 'logger'
import type { BlockEntity } from '@logseq/libs/dist/LSPlugin'
import type { LogseqDB } from '../data/bygonz'
import { getAllBlockVMs } from '../data/bygonz'
import { catchAndDisplayError, wrapWithCatchAndDisplayError } from '../utils'
import { INLINE_BLOCK_MACRO, ensureBygonzButtonInContent } from './ui-utils'
import { bygonzLoad } from '@/data/update-manager'
import { modifyBlocksWithoutChangeHandler } from '@/data/data-utils'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export function registerSlashCommands() {
  // logseq.Editor.registerSlashCommand('bygonz', async (event) => {
  //   try {
  //     const maybeBygonzUrl = (await logseq.Editor.getEditingBlockContent()).trim()
  //     DEBUG('[ /bygonz ] called', { event, maybeBygonzUrl })
  //     const bygonzUrl = new URL(maybeBygonzUrl)
  //     if (!bygonzUrl.host === 'bygonz')
  //       throw new Error(`Invalid url host: ${bygonzUrl.host}`)
  //     const params = bygonzUrl.searchParams
  //     if (!params.get('block'))
  //       throw new Error(`Missing UUID in url: ${JSON.stringify([...params.entries()])}`)
  //     if (!isValidUUID(params.get('block'))) {
  //       ERROR(`Not a valid block UUID: ${params.get('block')}`)
  //       return
  //     }
  //     const bygonzID = maybeBygonzUrl
  //     const currentBlock = await logseq.Editor.getCurrentBlock()
  //     if (!currentBlock)
  //       throw new Error('Slash command but no current block?!') // HACK: use event->block

  //     const targetBlock = await logseq.Editor.insertBlock(
  //       currentBlock.uuid, '🚀 *Fetching ...*',
  //       { sibling: true, focus: false, customUUID: bygonzID, properties: { bygonz: bygonzID } },
  //     )
  //     if (!targetBlock)
  //       throw new Error('Insert result is null')
  //     DEBUG('[ /bygonz ] Inserted target:', targetBlock)
  //     // DEBUG('Pinning bygonz ID', matchingVM.uuid)
  //     // await logseq.Editor.upsertBlockProperty(currentBlock.uuid, 'bygonz', matchingVM.uuid)
  //     // await bygonzLoad({ currentBlock })

  //     const matchingVM = (await getAllBlockVMs()).find(vm => vm.uuid === bygonzID)
  //     DEBUG('[ /bygonz ] gathered facts', bygonzID, { matchingVM, currentBlock })

  //     if (!matchingVM) {
  //       WARN('Slash ID not found in LogseqDB') // TODO: show message
  //       await logseq.Editor.updateBlock(targetBlock.uuid, '⚠️ *Not in local DB (yet)*'/* , { properties: targetBlock.properties } */)
  //       await logseq.Editor.upsertBlockProperty(targetBlock.uuid, 'bygonz', bygonzID) // HACK: workaround for the updateBlock>properties not working
  //     }
  //     else {
  //       await bygonzLoad({ currentBlock: targetBlock })
  //     }

  //     // TODO delete currentBlock (the one with the UUID)

  //     // /* const blocks: IBatchBlock[] =  */await loadBlockFromIPFS(maybeCid, currentBlock)
  //     // await logseq.Editor.insertBatchBlock(targetBlock.uuid, blocks, {
  //     //   sibling: false,
  //     // })
  //   }
  //   catch (err) {
  //     ERROR('[ /bygonz ] error:', err)
  //     void logseq.UI.showMsg(`Failed to load bygonz URL - ${err?.message ?? JSON.stringify(err)}`, 'error')
  //   }
  // })

  // logseq.Editor.registerSlashCommand('subscribe', wrapWithCatchAndDisplayError(async (event) => {
  //   const maybeCid = (await logseq.Editor.getEditingBlockContent()).trim()
  //   DEBUG('[ /subscribe ] called', { event, maybeCid })
  //   // TODO: if (!isValidCid(maybeCid)) {
  //   //   ERROR(`Not a valid CID: ${maybeCid}`)
  //   //   return
  //   // }

  //   const sub = new Subscription({
  //     type: 'ipfs',
  //     info: {
  //       // appCreator: appNameObj.creator,
  //       // appName: appNameObj.name,
  //       accountName: 'bygonztest',
  //       // logId: 'testlog-one',
  //       ipns: maybeCid,
  //     },
  //   })

  //   await logseqDB.addSubscription(sub)
  // }, '/subscribe'))
}

export async function checkDBChangeForBygonzUrl(txOp: string | undefined, blocks: BlockEntity[], logseqDB: LogseqDB) {
  await catchAndDisplayError(async () => {
    LOG('Checking for url:', { txOp, blocks })
    if (!blocks)
      return
    for (const { content, uuid: targetUuid } of blocks) {
      if (content?.startsWith('logseq://')) { // cheapest check I could think of
        let url: URL
        try {
          url = new URL(content.split('\n')[0]) // HACK: to clean up the props - like `\nid:: abc123-...`
        }
        catch (error) {
          DEBUG('[checkDBChangeForBygonzUrl] Not a valid URL: ', content, error)
          continue
        }

        LOG('Found logseq:// url:', url)
        if (url.host === 'bygonz') {
          if (url.pathname === '/open') {
            const pubCid = url.searchParams.get('pub')
            const bygonzID = url.searchParams.get('block')
            if (bygonzID && pubCid)
              await loadBlockFromUrl(targetUuid, pubCid, bygonzID, logseqDB)
            else void logseq.UI.showMsg(`Invalid bygonz url parameters: '${[...url.searchParams.entries()].toString()}'`, 'error')
          }
          else { void logseq.UI.showMsg(`Unhandled bygonz url type: '${url.pathname}'`, 'error') }
        }
      }
    }
  }, 'Failed to handle bygonz URL')
}

async function loadBlockFromUrl(targetUuid: string, pubCid: string, bygonzID: string, logseqDB: LogseqDB) {
  const subscriptions = await logseqDB.getSubscriptionArray()
  DEBUG('Checking for subscription:', pubCid, { subscriptions })
  if (!subscriptions.find(sub => sub.info.ipns === pubCid)) {
    DEBUG('Subscribing to', pubCid)
    await logseqDB.addSubscription(new Subscription({
      type: 'ipfs',
      info: {
        accountName: 'bygonztest', // HACK: get account name from... somewhere
        ipns: pubCid,
      },
    }))
  }

  await modifyBlocksWithoutChangeHandler(async () => {
    let targetBlock: BlockEntity | null
    if (targetUuid === bygonzID) {
      targetBlock = await logseq.Editor.getBlock(bygonzID)
      if (!targetBlock)
        throw new Error('Get target block result is null')
      DEBUG('Re-using current block:', { bygonzID, targetUuid })
      await logseq.Editor.exitEditingMode()
    }
    else {
      DEBUG('Inserting new block:', { bygonzID, targetUuid })
      targetBlock = await logseq.Editor.insertBlock(
        targetUuid, ensureBygonzButtonInContent('🚀 *Fetching ...*'),
        { sibling: true, focus: false, customUUID: bygonzID },
      )
      if (!targetBlock)
        throw new Error('Insert result is null')
      DEBUG('[loadBlockFromUrl] Inserted target:', targetBlock)
      await logseq.Editor.removeBlock(targetUuid)
      // TODO: make sure there is nothing lost (text after the URL? properties? children?)
    }

    const matchingVM = (await getAllBlockVMs()).find(vm => vm.uuid === bygonzID)
    DEBUG('[loadBlockFromUrl] gathered facts for:', bygonzID, { matchingVM, targetBlock })
    if (!matchingVM) {
      WARN('Slash ID not found in LogseqDB') // TODO: show message
      await logseq.Editor.updateBlock(targetBlock.uuid, `${INLINE_BLOCK_MACRO} ⚠️ *Not in local DB (yet)*` /* , { properties: targetBlock.properties } */)
    }
    else {
      await bygonzLoad({ currentBlock: targetBlock })
    }
  })
}

if (import.meta.hot) {
  import.meta.hot.accept('bygonz', (newFoo) => {
    console.log('BYGONZ MODULE RELOAD', newFoo)
    import.meta.hot?.invalidate()
  })
}
