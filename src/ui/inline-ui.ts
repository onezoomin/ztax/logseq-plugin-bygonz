import '@logseq/libs'

// import { initIPFS, loadBlockFromIPFS } from './data/ipfs'
import type { BlockEntity } from '@logseq/libs/dist/LSPlugin.user'
import { Logger } from 'logger'
import { acknowlegedRoots, bygonzSaveBlock } from '../data/update-manager'
import { css, wrapWithCatchAndDisplayError } from './../utils'
import { INLINE_BLOCK_MACRO, getBlockIfUUID, hasBygonzButton, rerenderBygonzButton, updateBlockContent } from './ui-utils'
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

// const popoverUUID = ''// '6391dcf7-80f5-4db9-adac-71724391ca53'
// async function bygonzOpenPopover(clickArgObj) {
//   const {
//     value,
//     id,
//     className,
//     dataset: {
//       onClick,
//       blockUuid,
//       slotId,
//     },
//   } = clickArgObj
//   DEBUG('buttonClicked', clickArgObj, { document, window, parent })
//   // if (popoverUUID) {
//   //   popoverUUID = blockUuid
//   //   await updateBygonzButtonOnBlock(popoverUUID)
//   // }
//   // popoverUUID = blockUuid

//   // // await bygonzSaveBlock(blockUuid)
//   // await updateBygonzButtonOnBlock(blockUuid)

//   // const LogseqDB = await getInitializedLogseqDB()
//   // const publications = await LogseqDB.publicationsTable.toArray()
//   // if (publications.length !== 1) {
//   //   // HACK: should check which publication to use?xxxxxx
//   //   throw new Error(`Invalid publication count: ${publications.length}`)
//   // }
//   // const publicationHash = publications[0].ipns
//   // const bygonzShareURL = `logseq://bygonz/open?pub=${publicationHash}&block=${blockUuid}`
//   // DEBUG('button share URL:', bygonzShareURL, { copy })

//   // logseq.showMainUI({ autoFocus: true })

//   // // // Copy with options
//   // copy(bygonzShareURL, {
//   //   debug: true,
//   //   message: 'Press #{key} to copy',
//   // })
//   // const uiMsgHTML = `${bygonzShareURL} is on your clipboard (if you select it and copy it fast enough)`
//   // void logseq.UI.showMsg(uiMsgHTML, 'success', {
//   //   key: 'copy-bygonz',
//   //   timeout: 13500,
//   // })
//   // void updateBygonzButtonOnBlock(blockUuid)
// }
function objToHTMLattributes(attributes: Record<string, any>) {
  let retString = ''
  for (const [eachAtt, eachVal] of Object.entries(attributes))
    retString += ` ${eachAtt}="${eachVal}"`
  DEBUG('objToHTMLattributes', { htmlAtts: retString })
  return retString
}
export function registerInlineUI() {
  logseq.provideModel({
    // bygonzOpenPopover,
  })
  logseq.App.onMacroRendererSlotted(({ slot, payload }) => {
    const { uuid } = payload
    const [type] = payload.arguments
    DEBUG('macro render', type, { slot, payload })
    if (!type?.startsWith(':bygonz'))
      return
    const buttonClasses = `${acknowlegedRoots.has(uuid) ? 'bg-green-200' : ''}`
    const bygonzBlockAttributes = {
      'class': 'bygonz-block-inline',
      'data-slot-id': slot,
      'data-block-uuid': payload.uuid,
      'data-on-Xclick': 'bygonzOpenPopover',
      'data-on-click': 'showBygonz',
    }
    const blockAttHTML = objToHTMLattributes(bygonzBlockAttributes)
    DEBUG('macro render', type, { slot, payload, bygonzBlockAttributes, blockAttHTML })
    return logseq.provideUI({
      key: `inline_${payload.uuid}`,
      slot,
      reset: true,
      template: `
        <div ${blockAttHTML}>

          <button class="${buttonClasses}">⬢</button>

        </div>
        `,
    })

    /* ${popoverUUID !== payload.uuid
      ? ''
    : `
      <div
        class="bygonz-popover"
        data-block-uuid="${payload.uuid}"
      >
        <button
          data-on-Xclick="bygonz"
          onclick="() => alert('test')"
        >⬢</button>
          </div>`} */
  })
  logseq.provideStyle(css`
    div[id^="_bygonz--inline"] {
      align-items: center;
      position:relative;
    }
    .bygonz-block-inline {
      position:relative;

      height: calc(var(--ct-line-height, 1.5) * 1em);
      width: calc(var(--ct-line-height, 1.5) * 1em);
      display: contents;
      /*min-width: auto;*/
    }
    .bygonz-block-inline > button {
      height: calc(var(--ct-line-height, 1.5) * 1em);
      width: calc(var(--ct-line-height, 1.5) * 1em);
      min-width: 100%;
      border: 2px solid #aaa;
      border-radius: 0.33rem;
      line-height: 1;
    }

    .bygonz-popover {
      position:absolute;
      top: calc(var(--ct-line-height, 1.5) * 1em + 0.2em);
      padding: 0.5em;
      /* height: calc(var(--ct-line-height, 1.5) * 1em);
      width: calc(var(--ct-line-height, 1.5) * 1em); */
      min-width: 120px;
      min-height: 80px;
      background-color: #fff;
      border: 2px solid #aaa;
      box-shadow: 2px 2px 4px rgba(0,0,0,0.4);
      border-radius: 0.33rem;
      display: hidden;
      z-index: 100;
    }
  `)

  logseq.Editor.registerBlockContextMenuItem('⬢ Bygonz', wrapWithCatchAndDisplayError(async (event) => {
    DEBUG('Context menu click', event)
    const { uuid } = event

    await bygonzSaveBlock(uuid)
    await ensureBygonzButton(uuid)
    // await rerenderBygonzButton(uuid)
  }, 'Bygonz menu'))
}

export async function ensureBygonzButton(blockOrUUID: string | BlockEntity) {
  const block = await getBlockIfUUID(blockOrUUID)

  if (!(await hasBygonzButton(block)))
    await updateBlockContent(block, `${INLINE_BLOCK_MACRO} ${block.content}`)
  else
    await rerenderBygonzButton(block.uuid)
}

// if (import.meta.hot) {
//   import.meta.hot.accept((newFoo) => {
//     VERBOSE('BYGONZ MODULE RELOAD', newFoo)
//     import.meta.hot?.invalidate()
//   })
// }
