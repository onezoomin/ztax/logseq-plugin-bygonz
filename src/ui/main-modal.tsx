import { createApp } from 'vue'
import { Logger } from 'logger'
import { getInitializedLogseqDB } from '../data/bygonz'
// @ts-expect-error
import App from './App.vue'
import { emitter } from './ui-utils'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export function registerMainModal() {
  const app = createApp(App)
  app.config.globalProperties.emitter = emitter
  const appInstance = app.mount('#app')
  LOG('Created and moutned app:', { app, appInstance })
  // logseq.showMainUI()

  logseq.provideModel({
    async showBygonz(event) {
      LOG('Opening main ui', event)
      if (event.dataset?.blockUuid) {
        const uuid = event.dataset.blockUuid

        const LogseqDB = await getInitializedLogseqDB()
        const publications = await LogseqDB.publicationsTable.toArray()
        if (publications.length !== 1) {
          // HACK: should check which publication to use?xxxxxx
          throw new Error(`Invalid publication count: ${publications.length}`)
        }
        const publicationHash = publications[0].ipns
        DEBUG('Found publication hash', publicationHash, { publications })

        emitter.emit('open', { block: { uuid, publicationHash } })
      }
      logseq.showMainUI()
    },
  })
  logseq.setMainUIInlineStyle({
    zIndex: 11,
  })
}
