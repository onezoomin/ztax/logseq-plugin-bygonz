import '@logseq/libs'

// import './index.css'

// import { initIPFS, loadBlockFromIPFS } from './data/ipfs'
import { Logger } from 'logger'
import { state as bygonzState, registerStateListener, toggles } from '../data/update-manager'
import { css, wrapWithCatchAndDisplayError } from './../utils'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const renderToolbarItems = () => {
  DEBUG('Rendering toolbar items', bygonzState)
  let stateIcon = '❔' // ideas: ⚪️ 🟢 🔄 🚀 📩 ⌛
  if (bygonzState === 'uninitialized')
    stateIcon = '⌛'
  if (bygonzState === 'loading')
    stateIcon = '🚀'
  if (bygonzState === 'pulling')
    stateIcon = '📩'
  if (bygonzState === 'disabled')
    stateIcon = '⚪️'
  if (bygonzState === 'warning')
    stateIcon = '⚠️'
  if (bygonzState === 'ready')
    stateIcon = '⬢'

  const realtime = toggles.realtime && ['ready', 'pulling'].includes(bygonzState)
  logseq.App.registerUIItem('toolbar', {
    key: 'template-plugin-open',
    template: `
        <div data-on-click="onToolbarClick" data-toggles="realtime" class="bygonz-toolbar ${realtime ? 'bg-green-200' : ''}">
          ${stateIcon} BYGONZ
        </div>
    `,
  })

  logseq.provideStyle(css`
    .bygonz-toolbar {
      opacity: 0.8;
      font-size: 30%;
      margin: 3px 3px 0;
      border: 1px solid #ccc;
      padding: 5px 7px;
      border-radius: 5px;
      cursor: pointer;
    }

    .bygonz-toolbar:hover {
      opacity: 1;
    }
  `)
}

export function registerToolbarItems() {
  logseq.provideModel({
    onToolbarClick: wrapWithCatchAndDisplayError(async (event) => {
      DEBUG('toolbar click', event)
      logseq.showMainUI()

      // const toggle = event.dataset.toggles
      // toggles[toggle] = !toggles[toggle]
      // if (toggle === 'realtime') {
      //   if (toggles[toggle]) {
      //     LOG('Enabling realtime listener', logseq.settings)
      //     // logseq.DB.onChanged((event) => {
      //     //   VERBOSE('onChange listener', event)
      //     //   if (!toggles[toggle]) return
      //     //   void onDBChange(event).catch(err => ERROR('onDBChange error', err))
      //     // })
      //   } else {
      //     LOG('Disabling realtime listener')
      //     // don't know how to disable, so we just check in the listener above
      //   }
      // }
      // renderToolbarItems()
    }, 'Bygonz toolbar'),
  })
  registerStateListener(() => renderToolbarItems())

  renderToolbarItems()
}
