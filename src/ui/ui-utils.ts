import type { BlockEntity } from '@logseq/libs/dist/LSPlugin'
import mitt from 'mitt'
import { modifyBlocksWithoutChangeHandler } from '../data/data-utils'

export const INLINE_BLOCK_MACRO = '{{renderer :bygonz}}'

export const emitter = mitt()

export async function hasBygonzButton(blockOrUUID: string | BlockEntity) {
  const block = await getBlockIfUUID(blockOrUUID)
  return block.content.includes(INLINE_BLOCK_MACRO)
}

export async function rerenderBygonzButton(blockOrUUID: string | BlockEntity) {
  const block = await getBlockIfUUID(blockOrUUID)

  // TODO: safety check: editing?

  // Hack: to re-render block: add invisible space and remove it

  await updateBlockContent(block, `${block.content}ㅤ`)
  await updateBlockContent(block, block.content)
}

export function ensureBygonzButtonInContent(content: string) {
  if (!content.includes(INLINE_BLOCK_MACRO))
    return `${INLINE_BLOCK_MACRO} ${content}`
  else
    return content
}

export function removeBygonzButtonInContent(content: string) {
  return content.replaceAll(/\{\{ *renderer *:bygonz *\}\} ?/g, '') // Remove bygonz button (and a space after if there is one)
}

export async function getBlockIfUUID(blockOrUUID: BlockEntity | string): Promise< BlockEntity> {
  if (typeof blockOrUUID === 'string') {
    const block = await logseq.Editor.getBlock(blockOrUUID)
    if (!block)
      throw new Error(`Could not find block: ${blockOrUUID}`)
    return block
  }
  return blockOrUUID
}

export async function updateBlockContent(block: BlockEntity, content: string) {
  await modifyBlocksWithoutChangeHandler(async () => {
    await logseq.Editor.updateBlock(
      block.uuid,
      content,
      { properties: block.properties }, // FIXME: doesn't work, needs PR on logseq 💁
    )
  })
}
