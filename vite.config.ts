import path from 'path'
import { defineConfig } from 'vite'
import logseqDevPlugin from 'vite-plugin-logseq'
import { globalPolyfill } from 'vite-plugin-global-polyfill'
import Vue from '@vitejs/plugin-vue'
import VueMacros from 'unplugin-vue-macros/vite'
// import Unocss from 'unocss/vite'
// import AutoImport from 'unplugin-auto-import/vite'
// import globals from "rollup-plugin-node-globals";
// import nodePolyfills from 'rollup-plugin-node-polyfills'
import Inspect from 'vite-plugin-inspect'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [
    Inspect(), // -> http://localhost:5173/__inspect/
    logseqDevPlugin({ entry: 'src/main.tsx' }),

    VueMacros({
      plugins: {
        vue: Vue({
          // reactivityTransform: resolve(__dirname, 'src/ui'),
        }),
      },
    }),
    globalPolyfill(),
    // @ ts-expect-error
    // nodePolyfills({}),
    // { ...globals(), name: 'rollup-plugin-node-globals' },

    // // https://github.com/antfu/unplugin-auto-import
    // AutoImport({
    //   imports: [
    //     'vue',
    //     'vue/macros',
    //     // 'vue-router',
    //     '@vueuse/core',
    //   ],
    //   dts: true,
    //   dirs: [
    //     './src/composables',
    //   ],
    //   vueTemplate: true,
    // }),

    // https://github.com/antfu/unocss
    // see unocss.config.ts for config
    // Unocss(),
  ],

  // Makes HMR available for development
  build: {
    // target: 'es2020',
    target: 'esnext',
    // minify: "esbuild",
  },
  esbuild: {
    exclude: 'bygonz',
  },
  optimizeDeps: {
    esbuildOptions: { target: 'esnext' },
    // https://vitejs.dev/config/server-options.html#server-watch
    exclude: [
      '* > bygonz',
      'bygonz > *',
      'bygonz',
      'service-worker/',
      'node_modules/',
      'node_modules/*',
      '../ztax/',
      '../ztax/*',
      '/home/manu/dev/tamera/ztax/',
      '/home/manu/dev/tamera/ztax/*',
    ],
  },
  ssr: {
    external: ['bygonz'],
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      // 'bygonz': path.resolve(__dirname, '../ztax/src/bygonz/dist/bygonz.cjs'), // dist/bygonz.js
      // 'bygonz': path.resolve(__dirname, '../ztax/src/bygonz/src/index.ts'), // dist/bygonz.js
    },
  },

  define: {
    'process.env': {},
  },

  // BYGONZ HOT RELOAD - https://vitejs.dev/config/server-options.html#server-watch
  server: {
    strictPort: true,
    // port: 12345,
    watch: {
      ignored: [
        // '!**/node_modules/bygonz/**',
        '!../ztax/**',
      ],
    },
    fs: {
      // Allow serving files from ztax dist (namingly, the WebWorker and it's rust wasm part)
      allow: ['.', '../ztax/dist/'],
    },
  },
})
